import { PublicKey } from './public-key.js';

import { Address } from './address.js';

import {
	binToHex,
	decodePrivateKeyWif,
	encodePrivateKeyWif,
	generatePrivateKey,
	hexToBin,
	isHex,
	secp256k1,
} from '@bitauth/libauth';

import type {
	WalletImportFormatType,
} from '@bitauth/libauth';

import {
	randomBytes,
} from 'crypto';

/**
* Private Key entity.
*/
export class PrivateKey
{
	// The raw bytes representing the Private Key.
	private readonly bytes: Uint8Array;

	/**
	 * Construct a new Private Key.
	 *
	 * @throws {Error} If Private Key cannot be constructed.
	 *
	 * @param bytes {Uint8Array} The raw bytes (32) for the Private Key.
	 */
	constructor(bytes: Uint8Array)
	{
		// Ensure that exactly 32 Bytes are given.
		if(bytes.length !== 32)
		{
			throw(new Error(`Expected 32 bytes for Private Key (${bytes.length} bytes given).`));
		}

		this.bytes = bytes;
	}

	/**
	 * Derive the public key from this private key.
	 *
	 * @throws {Error} If Public Key cannot be derived.
	 *
	 * @returns {PublicKey} The derived Public Key.
	 */
	public derivePublicKey(): PublicKey
	{
		// Attempt to derive the public key.
		const deriveResult = secp256k1.derivePublicKeyCompressed(this.bytes);

		// If a string is returned, this indicates an error...
		if(typeof deriveResult === 'string')
		{
			throw(new Error(deriveResult));
		}

		// Return the derived Public Key.
		return new PublicKey(deriveResult);
	}

	/**
	 * Derive the Address from this Private Key.
	 *
	 * @throws {Error} If Address cannot be derived.
	 *
	 * @returns {Address} The derived Address.
	 */
	public deriveAddress(): Address
	{
		// Attempt to derive the public key and then the address.
		const address = this.derivePublicKey().deriveAddress();

		// Return the derived Address entity.
		return address;
	}

	/**
	 * Sign a message hash with this Private Key using a Compact Signature.
	 *
	 * @param messageHash {Uint8Array} The hash of the message.
	 *
	 * @throws {Error} If message hash cannot be signed.
	 *
	 * @returns {Uint8Array} The generated signature.
	 */
	public signMessageHashCompact(messageHash: Uint8Array): Uint8Array
	{
		// Attempt to sign the message.
		const signResult = secp256k1.signMessageHashCompact(this.bytes, messageHash);

		// If a string is returned, this indicates an error...
		if(typeof signResult === 'string')
		{
			throw(new Error(signResult));
		}

		// Return the signature.
		return signResult;
	}

	/**
	 * Sign a message hash with this Private Key using a Schnorr Signature.
	 *
	 * @param messageHash {Uint8Array} The hash of the message.
	 *
	 * @throws {Error} If message hash cannot be signed.
	 *
	 * @returns {Uint8Array} The generated signature.
	 */
	public signMessageHashSchnorr(messageHash: Uint8Array): Uint8Array
	{
		// Attempt to sign the message.
		const signResult = secp256k1.signMessageHashSchnorr(this.bytes, messageHash);

		// If a string is returned, this indicates an error...
		if(typeof signResult === 'string')
		{
			throw(new Error(signResult));
		}

		// Return the signature.
		return signResult;
	}

	/**
	 * Sign a message hash with this Private Key using a DER Signature.
	 *
	 * @param messageHash {Uint8Array} The hash of the message.
	 *
	 * @throws {Error} If message hash cannot be signed.
	 *
	 * @returns {Uint8Array} The generated signature.
	 */
	public signMessageHashDER(messageHash: Uint8Array): Uint8Array
	{
		const signResult = secp256k1.signMessageHashDER(this.bytes, messageHash);

		// If a string is returned, this indicates an error...
		if(typeof signResult === 'string')
		{
			throw(new Error(signResult));
		}

		// Return the signature.
		return signResult;
	}

	/**
	 * Convert the raw bytes of this Private Key to hexadecimal encoding.
	 *
	 * @returns {string} The raw bytes of the Private Key as a hexadecimal string.
	 */
	public toHex(): string
	{
		// Return the hexadecimal encoded bytes  of this Private Key.
		return binToHex(this.bytes);
	}

	/**
	 * Convert this Private Key to a raw 32 byte representation.
	 *
	 * @returns {Uint8Array} The raw Private Key as a Uint8Array.
	 */
	public toUint8Array(): Uint8Array
	{
		// Return the raw bytes for this Private Key.
		return this.bytes;
	}

	/**
	 * Convert this Private Key to a WIF format.
	 *
	 * @param type {WalletImportFormatType} The type of WIF to create.
	 *
	 * @returns {string} The Private Key in WIF format.
	 */
	public toWif(type: WalletImportFormatType = 'mainnet'): string
	{
		// Return the encoded Private Key WIF.
		return encodePrivateKeyWif(this.bytes, type);
	}

	/**
	 * Returns the Private Key as a mainnet WIF.
	 *
	 * @returns {string} The Private Key as a mainnet WIF.
	 */
	public toString(): string
	{
		// Return the encoded Mainnet Private Key WIF.
		return this.toWif();
	}

	/**
	 * Create a Private Key Entity from the given bytes (as hexadecimal string).
	 *
	 * @param privateKeyHex {string} Raw Private Key bytes encoded as hexadecimal string.
	 *
	 * @throws {Error} If Private Key could not be created.
	 *
	 * @returns {PrivateKey} The created Private Key Entity.
	 */
	public static fromHex(privateKeyHex: string): PrivateKey
	{
		// Ensure that the given string is hex.
		if(!isHex(privateKeyHex))
		{
			throw(new Error(`The given string (${privateKeyHex}) is not valid hexadecimal.`));
		}

		// Convert the hex to binary (Uint8Array).
		const privateKeyBin = hexToBin(privateKeyHex);

		// Create a Private Key Entity from the binary.
		return new PrivateKey(privateKeyBin);
	}

	/**
	 * Create a Private Key from a given WIF (Note that the network information from the WIF will be lost).
	 *
	 * @throws {Error} If Private Key cannot be created.
	 *
	 * @returns {PrivateKey} The Private Key
	 */
	public static fromWIF(wif: string): PrivateKey
	{
		// Attempt to decode the WIF.
		const decodeResult = decodePrivateKeyWif(wif);

		// If a string is returned, this indicates an error...
		if(typeof decodeResult === 'string')
		{
			throw(new Error(decodeResult));
		}

		// Return a new Private Key entity.
		return new PrivateKey(decodeResult.privateKey);
	}

	/**
	 * Generate a random Private Key.
	 *
	 * @returns {PrivateKey} The randomly generated Private Key
	 */
	public static generateRandom(): PrivateKey
	{
		const bytes = generatePrivateKey(() => randomBytes(32));

		return new PrivateKey(bytes);
	}
}
