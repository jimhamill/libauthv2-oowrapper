# General Protocols - Library Template

General Protocol template for scaffolding libraries. Includes scaffold for:

1. General Protocols Lint Configuration.
2. General Protocols Spellcheck Configuration.
3. Documentation Generation using JSDocs + BetterDocs + DashDocs template.
4. Typescript + Rollup Build Process.
5. Jest Unit Tests and E2E Tests.
6. Pre-push Githooks for Lint, Spellcheck Tests and Build.
7. Gitlab Continuous Integration File (`.gitlab-ci.yml`).

## Setup Instructions (REMOVE ME)

List of steps to prepare template:

1. Copy and paste this template over empty repository.
2. Open package.json and modify any `TODO` string.
3. Open CODEOWNERS and modify.
4. Modify this README.md file to reflect Library Usage.

## Usage

### Installation

Installing the library.

```sh
npm install @generalprotocols/TODO
```

### Usage

Before you can use the Price Oracle functionality in your project, you need to import it to your project:

```js
// Import the library.
import { ... } from '@generalprotocols/TODO';
```
